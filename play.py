import webapp2

# method='post' changes the form default from GET to POST, which changes
# the function of the form from requesting to posting. With POST, the url
# will no longer change to contain the query of the input.
form = """
<form method='post'>
    What is your birthday?
    <br>
    <input type='text' name='month' value='%(month)s' placeholder='month'>
    <input type='text' name='day' value='%(day)s' placeholder='day'>
    <input type='text' name='year' value='%(year)s' placeholder='year'>
    <div style='color: red'>%(error)s</div>
    <input type='submit'>
</form>
"""

months = ['January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December']

# list comprehension: (__expression__ | __loop(s)__ | __Condition/Filter__)
month_abbrev = dict((month[:3].upper(), month) for month in months)

class MainPage(webapp2.RequestHandler):
    def write_form(self, error='', day='', month='', year=''):
        self.response.out.write(form %{"error": error,
                                       "month": month,
                                       "day": day,
                                       "year": year})

    def get(self):
        self.write_form()

    def post(self):
        # These variables represent what the user entered and use the inputs'
        # name attribute to acquire the information to run in the function.
        user_day = escape_html(self.request.get('day'))
        user_month = escape_html(self.request.get('month'))
        user_year = escape_html(self.request.get('year'))

        ## These variables represent whether the information entered was
        # validated or not
        day = valid_day(user_day)
        month = valid_month(user_month)
        year = valid_year(user_year)

        # If all are not validate, return the form again
        # If all are validated, return a response
        if not (day and month and year):
            self.write_form("Your input is not valid.", user_day, user_month, user_year)
        else:
            # Method for redirects w/ Google App Engine
            # redirects to the /thanks URL (www.example.com/thanks)
            self.redirect('/thanks')

# Handles redirecting the valid form complete response to an URL you can visit
class ThanksHandler(webapp2.RequestHandler):
    def get(self):
        self.response.out.write('Thanks, your information is valid.')

def valid_month(month):
    if month:
        short_month = month[:3].upper()
        return month_abbrev.get(short_month)

def valid_day(day):
    if (day.isdigit()) and (32 > int(day) > 0): return int(day)
    if day.isdigit() is False: return None

def valid_year(year):
    if year and year.isdigit():
        year = int(year)
        if 2021 > year > 1899:
            return year

# If a user types in any of the below characters, this function will replace
# them will the html escape version.
# Alternate: You can 'import cgi' (comes with python) and use
# 'cgi.escape(s, quote = True)' to replace the for loop in this function.
# It's best to use built-in functions than your own as they're tested and safe.
def escape_html(s):
    for (i, o) in (('&', '&amp;'),
                    ('>', '&gt;'),
                    ('<', '&lt;'),
                    ('"', '&quot;')):
        s = s.replace(i, o)
    return s

app = webapp2.WSGIApplication([('/', MainPage), ('/thanks', ThanksHandler)], debug=True)
